/***********************************************************************
[17-Apr-1995]
***********************************************************************/

#include <config.h>

#if HAVE_STDIO_H
#include <stdio.h>
#endif

#if HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

int
main(argc,argv)
int argc;
char* argv[];
{
    int c;

    while ((c = getchar()) != EOF)
	(void)putchar(c);

    exit (EXIT_SUCCESS);
    return (EXIT_SUCCESS);
}
