:# Build texpty with Microsoft C 5.1, 6.0, or 7.0.  The large memory
:# model is necessary (compact model gives stack overflow for all
:# stack sizes from 0 to 64KB).
:# [07-Jun-1995]

:# -AC (compact memory model), -F 5000h (stack size)
cl -o texpty.exe -AC -DUSER=\"beebe\" -DHOSTNAME=\"math.utah.edu\" -F 5000 texpty.c
del texpty.obj
