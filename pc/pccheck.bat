:# Check the results of the IBM PC version of texpty
:# Only differences in comment banners should be expected
:# [13-Jun-1995]

del check*.err
del check*.out

texpty -l check001.err check001.in >check001.out
diff check001.eok check001.err
diff check001.ok check001.out

texpty -l check002.err check002.in >check002.out
diff check002.eok check002.err
diff check002.ok check002.out

texpty -l check003.err check003.in >check003.out
diff check003.eok check003.err
diff check003.ok check003.out

texpty -l check004.err check004.in >check004.out
diff check004.eok check004.err
diff check004.ok check004.out

texpty -l check005.err check005.in >check005.out
diff check005.eok check005.err
diff check005.ok check005.out

texpty -l check006.err check006.in >check006.out
diff check006.eok check006.err
diff check006.ok check006.out

texpty -l check007.err check007.in >check007.out
diff check007.eok check007.err
diff check007.ok check007.out

texpty -l check008.err check008.in >check008.out
diff check008.eok check008.err
diff check008.ok check008.out

texpty -l check009.err check009.in >check009.out
diff check009.eok check009.err
diff check009.ok check009.out

texpty -l check010.err check010.in >check010.out
diff check010.eok check010.err
diff check010.ok check010.out

texpty -l check011.err check011.in >check011.out
diff check011.eok check011.err
diff check011.ok check011.out

