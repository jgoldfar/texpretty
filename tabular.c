/***********************************************************************
Here is a brief, though incompletely specified, grammar of a LaTeX /
AmSTeX / LAmSTeX tabular environment, where as usual, * stands for 0 or
more, and ? for 0 or 1, except in literal quoted strings:

TabularEnvironment	:= BeginTabular Rows EndTabular

BeginTabular		:= \begin{tabular} |
			   \begin{tabular*} |
			   \xxx

EndTabular		:= \end{tabular} |
			   \end{tabular*} |
			   \endxxx

Rows			:= Row |
			   Row RowSeparator Rows |
			   \hline Rows

Row			:= Cell |
			   Cell & Row |
			   Empty

RowSeparator		:= "\\" WhiteSpace* "*"? Len?

Len			:= [ WhiteSpace* Dimen WhiteSpace* ]

Cell			:= WhiteSpace* Other? WhiteSpace*

Whitespace		:= Blank |
			   Tab |
			   Comment

Comment			:= % Any* Newline

For simplicity, we assume that these environments are not nested
(i.e. no Cell is itself a tabular environment).

[16-Jun-1995]
***********************************************************************/

#include <config.h>

#if HAVE_STDIO_H
#include <stdio.h>
#endif

#if HAVE_STRING_H
#include <string.h>			/* for strlen() declaration */
#endif

#if HAVE_CTYPE_H
#include <ctype.h>
#endif

#include "stdc.h"			/* NB: must be included AFTER */
					/* all system header files, to */
					/* avoid inconsistent definitions */
					/* of const */

#if defined(FLEX_SCANNER)
#define LEXEOF(c)	((c) == EOF)	/* flex end-of-file test */
#else
#define LEXEOF(c)	((c) == 0)	/* lex end-of-file test */
#endif

#if STDC
typedef void* VOIDP;
#else
typedef char* VOIDP;
#endif

#define NOOP	/* NO-OP */

/* dynamically-allocated static arrays for copy_tabular() and descendants */

static int		column = 0;
static int		max_column = 0;

static int		*column_width = (int*)NULL;
static int		len_column_width = 0;

static char		*tabular_buffer = (char*)NULL;
static int		len_tabular_buffer = 0;

static int		tabular_buffer_next = 0;
static int		tabular_buffer_end = 0;

extern int		in_verbatim;
extern char		*yytext;

static size_t		out_bytepos = 0;



#if !defined(COLUMNCHUNK)
#define COLUMNCHUNK	25
#endif

#if !defined(TABLECHUNK)
#define TABLECHUNK	500
#endif


extern void		adjust_environment_level ARGS((int n,
						       const char *command));
extern void		copy_tabular ARGS((void));
extern void		delete_spaces ARGS((void));
extern VOIDP		get_memory ARGS((VOIDP ptr, size_t size));
extern int		input_char ARGS((void));
extern int		last_char ARGS((int offset));
extern void		out_char ARGS((int c));
extern void		out_indentation ARGS((void));
extern void		out_linebreak ARGS((void));
extern void		out_string ARGS((const char *s));

#define out_verbatim_char(c) out_char(c)

static void		copy_tabular_to_memory ARGS((void));
static void		copy_tabular_to_output ARGS((void));
static size_t		get_cell ARGS((void));
static size_t		get_cell_separator ARGS((void));
static void		get_column_widths ARGS((void));
static size_t		get_optional_argument ARGS((void));
static size_t		get_row ARGS((void));
static size_t		get_row_separator ARGS((void));
static size_t		get_whitespace ARGS((void));
static VOIDP		grow_buffer ARGS((VOIDP buffer, size_t *buffer_size,
					  size_t extra_elements,
					  size_t element_size));
static void		new_column ARGS((void));
static size_t		out_cell ARGS((void));
static size_t		out_cell_separator ARGS((void));
static size_t		out_ignorable_whitespace ARGS((void));
static size_t		out_optional_argument ARGS((void));
static size_t		out_position ARGS((void));
static size_t		out_row ARGS((void));
static size_t		out_row_separator ARGS((void));
static void		out_tabular_char ARGS((int c));
static void		out_tabular_string ARGS((const char *s, size_t n));
static size_t		out_whitespace ARGS((void));
static void		set_tabular_column_width ARGS((int n));

#define Advance_Position()	tabular_buffer_next++

#define Current_Char()		tabular_buffer[Current_Position()]

#define Current_Position()	tabular_buffer_next

#define Previous_Char()		tabular_buffer[Current_Position() - 1]

#define In_Tabular()	(Current_Position() < tabular_buffer_end)

#define Is_Command(c)	((Current_Char() == c) && (Current_Position() > 0) && \
			 (Previous_Char() != '\\'))

void
copy_tabular(VOID_ARG)
{
    /* Copy up to matching \end{tabular}, \end{tabular*}, and reformat */
    /* the rows to uniform column widths for better readability. */

    copy_tabular_to_memory();		/* to facilitate subsequent scanning */

    get_column_widths();

    copy_tabular_to_output();
}


static void
copy_tabular_to_memory(VOID_ARG)
{
    register int c;
    static char *endcommand = (char*)NULL;
    static size_t len_endcommand = 0;
    size_t len;
    register int matches;
    char *p;

    len = strlen(yytext) - sizeof("begin") + sizeof("end") + 1;

    if (len > len_endcommand)
    {					/* create or enlarge buffer */
	endcommand = (char*)get_memory(endcommand, len);
	len_endcommand = len;
    }
    (void)strcpy(endcommand,"\\end");
    (void)strcpy(&endcommand[4], (const char*)&yytext[6]);

    p = strchr(endcommand,'}');		/* remove any second argument */
    if (p != (char*)NULL)
	p[1] = '\0';

    c = 0;			/* initialize to keep optimizers happy */
    Current_Position() = 0;		/* start table parse */
    matches = 0;
    tabular_buffer_end = 0;
    for (NOOP; endcommand[matches] != '\0'; NOOP)
    {
	c = input_char();
	if (LEXEOF(c))
	    break;

	if (Current_Position() >= (len_tabular_buffer - 1)) /* space for c + NUL */
	{				/* time to enlarge tabular_buffer[] */
	    len_tabular_buffer += TABLECHUNK;
	    tabular_buffer = (char*)get_memory(tabular_buffer,len_tabular_buffer);
	}

	Current_Char() = (char)c;
	Advance_Position();

	if (endcommand[matches] == (char)c)
	    matches++;
	else
	    matches = 0;
    }

    Current_Char() = '\0'; /* terminate for later scans */

    tabular_buffer_end = LEXEOF(c) ? (Current_Position() - 1) :
	Current_Position() - matches;	/* index of \end{tabular} */
}


static void
copy_tabular_to_output(VOID_ARG)
{
    const char *endcommand;
    size_t row;

    endcommand = &tabular_buffer[tabular_buffer_end];
    in_verbatim = 1;
    out_bytepos = 0;
    out_tabular_char((int)'\n');
    Current_Position() = 0;		/* start table parse */
    while ((row = out_row()) > 0)
    {					/* process Rows */
	if (strncmp(&tabular_buffer[row],"\\hline",6) != 0)
	    (void)out_row_separator();
    }
    adjust_environment_level(-1,endcommand);
    out_linebreak();
    out_indentation();
    out_string(endcommand);
    in_verbatim = 0;
}


static size_t
get_cell(VOID_ARG)
{	/* return index of non-blank start of next cell; Current_Position() */
	/* does not advance beyond & or \\ */
    size_t cell_start;
    size_t k;

    new_column();
    (void)get_whitespace();
    cell_start = (size_t)Current_Position();
    for (;;)
    {
	if (!In_Tabular())
	    break;			/* end of Tabular */
	else if (Is_Command('&'))
	    break;			/* end of Cell, not at end of Row */
	else if (strncmp(&Current_Char(),"\\\\",2) == 0)
	    break;			/* end of Cell, end of Row */
	else if (strncmp(&Current_Char(),"\\hline",6) == 0)
	{
	    set_tabular_column_width(6); /* need to handle this special case */
	    break;			/* end of Cell, end of Row */
	}
	else
	{
	    Advance_Position();
	    (void)get_whitespace();
	}
    }

    /* Compute Cell width, ignoring leading and trailing space.	 This
       will be an overestimate if the Cell contains an embedded comment,
       but that is rare. */
    for (k = Current_Position() - 1;
	 (k >= cell_start) && isspace(tabular_buffer[k]); --k)
	NOOP;

    /* Ignore cells containing \multicolumn{}{}{} */
    if (strncmp(&tabular_buffer[cell_start],"\\multicolumn",12) != 0)
	set_tabular_column_width((int)(k + 1 - cell_start));
    return (cell_start);
}


static size_t
get_cell_separator(VOID_ARG)
{   /* return index of & (advancing Current_Position() past it) or else 0 */
    (void)get_whitespace();
    if (Is_Command('&'))
    {
	Advance_Position();
	return ((size_t)(Current_Position() - 1));
    }
    else
	return ((size_t)0);
}


static void
get_column_widths(VOID_ARG)
{
    /*******************************************************************
    At entry to this function, tabular_buffer[] contains all of the text,
    EXCEPT for the BeginTabular prefix, and
    tabular_buffer[tabular_buffer_end] is the start of the EndTabular.

    During the scans below, the first non-blank in the first Cell of a
    Row starts at get_row(), and ends at
    tabular_buffer[tabular_buffer_next-1].  tabular_buffer[] itself is NOT
    modified once it has been collected by copy_tabular_to_memory().
    *******************************************************************/

    size_t row;

    max_column = 0;
    Current_Position() = 0;		/* start table parse */
    while ((row = get_row()) > 0)
    {					/* process Rows */
	if (strncmp(&tabular_buffer[row],"\\hline",6) != 0)
	    (void)get_row_separator();
    }
}


static size_t
get_optional_argument(VOID_ARG)
{
    size_t n;

    (void)get_whitespace();
    if (Current_Char() == '[')
    {
	n = (size_t)Current_Position();
	Advance_Position();
	(void)get_whitespace();
	while (In_Tabular() &&  /* '[' for balance */(Current_Char() != ']'))
	{
	    Advance_Position();
	    (void)get_whitespace();
	}
	if (Current_Char() == ']')
	    Advance_Position();
    }
    else
	n = 0;
    return (n);
}


static size_t
get_row(VOID_ARG)
{	/* return index of next row (advancing Current_Position() to end */
	/* of row, just before \\ or \end..., or after \hline), or else */
	/* 0 (if no more rows) */

    size_t first_cell;

    if (!In_Tabular())
	return (0);

    column = 0;				/* global start of new row */
    first_cell = get_cell();
    if (strncmp(&tabular_buffer[first_cell],"\\hline",6) == 0)
	Current_Position() = first_cell + 6; /* special case */
    else
    {
	while (get_cell_separator() > 0)
	    (void)get_cell();
    }


#if 0
    {					/* DEBUG OUTPUT */
	int k;

	(void)fprintf(stderr,"Column widths:");
	for (k = 1; k <= max_column; ++k)
	    (void)fprintf(stderr, " %d", column_width[k]);
	(void)fprintf(stderr,"\n");
    }
#endif

    return (first_cell);
}


static size_t
get_row_separator(VOID_ARG)
{   /* return index of \\*[...] (advancing Current_Position() past it), or 0 */
    size_t separator_start;

    (void)get_whitespace();
    if (strncmp(&Current_Char(),"\\\\",2) == 0)
    {
	separator_start = (size_t)Current_Position();
	Current_Position() += 2;
	(void)get_whitespace();
	if (Current_Char() == '*')
	    Advance_Position();
	(void)get_optional_argument();
    }
    else
	separator_start = (size_t)0;
    return (separator_start);
}


static size_t
get_whitespace(VOID_ARG)
{
    size_t n;

    n = (size_t)((In_Tabular() && isspace(Current_Char())) ?
		 Current_Position() : 0);

    while (In_Tabular() && isspace(Current_Char()))
	Advance_Position();			/* skip whitespace */

    if (Is_Command('%'))
    {					/* found comment, so skip it too */
	do				/* advance to newline */
	{
	    Advance_Position();
	    if (!In_Tabular())
		break;
	} while (Current_Char() != '\n');
	(void)get_whitespace();
    }
    return (n);
}


#if STDC
static VOIDP
grow_buffer(VOIDP buffer, size_t *buffer_size, size_t extra_elements,
	    size_t element_size)
#else
static VOIDP
grow_buffer(buffer, buffer_size, extra_elements, element_size)
VOIDP buffer;
size_t *buffer_size;
size_t extra_elements;
size_t element_size;
#endif
{					/* grow a buffer with zeroed memory */
    char *end;
    VOIDP new_buffer;

    end = (char*)buffer + *buffer_size * element_size;
    *buffer_size += extra_elements;
    new_buffer = get_memory(buffer, *buffer_size * element_size);
    if (buffer == (VOIDP)NULL)
	end = (char*)new_buffer;

#if STDC
    (void)memset((void*)end, 0, extra_elements * element_size);
#else
    (void)bzero(end, (int)(extra_elements * element_size));
#endif

    return (new_buffer);
}


static void
new_column(VOID_ARG)
{
    if (column >= (len_column_width - 1))
	column_width = (int*)grow_buffer((VOIDP)column_width,
					       (size_t*)&len_column_width,
					       (size_t)COLUMNCHUNK,
					       sizeof(int));
    column++;
}


static size_t
out_cell(VOID_ARG)
{	/* return index of non-blank start of next cell; Current_Position() */
	/* does not advance beyond & or \\ */
    size_t cell_start;
    size_t cell_width;
    size_t out_start;

    new_column();
    (void)out_ignorable_whitespace();
    out_start = out_position();
    cell_start = (size_t)Current_Position();
    for (;;)
    {
	if (!In_Tabular())
	    break;			/* end of Tabular */
	else if (Is_Command('&'))
	    break;			/* end of Cell, not at end of Row */
	else if (strncmp(&Current_Char(),"\\\\",2) == 0)
	    break;			/* end of Cell, end of Row */
	else if (strncmp(&Current_Char(),"\\hline",6) == 0)
	    break;			/* end of Cell, end of Row */
	else
	{
	    out_tabular_char(Current_Char());
	    Advance_Position();
	    (void)out_whitespace();
	}
    }

    for (cell_width = (size_t)(out_position() - out_start);
	 cell_width <= (size_t)column_width[column]; ++cell_width)
	out_tabular_char((int)' ');	/* supply right fill plus one blank */

    return (cell_start);
}


static size_t
out_cell_separator(VOID_ARG)
{   /* return index of & (advancing Current_Position() past it) or else 0 */
    (void)out_whitespace();
    if (Is_Command('&'))
    {
	if (!isspace(last_char(0)))
	    out_tabular_char((int)' ');
	out_tabular_char((int)'&');
	out_tabular_char((int)' ');
	Advance_Position();
	return ((size_t)(Current_Position() - 1));
    }
    else
	return ((size_t)0);
}


static size_t
out_ignorable_whitespace(VOID_ARG)
{   /* output comments verbatim, but discard all other whitespace  */
    size_t n;

    n = (size_t)((In_Tabular() && isspace(Current_Char())) ?
		 Current_Position() : 0);

    for (NOOP; In_Tabular() && isspace(Current_Char()); Advance_Position())
	NOOP;				/* discard whitespace */

    if (Is_Command('%'))
    {					/* found comment, so output it */
	out_tabular_char((int)Current_Char());
	do				/* advance to newline */
	{
	    Advance_Position();
	    if (!In_Tabular())
		break;
	    out_tabular_char((int)Current_Char());
	} while (Current_Char() != '\n');
	(void)out_whitespace();
    }
    return (n);
}


static size_t
out_optional_argument(VOID_ARG)
{
    size_t n;

    (void)out_whitespace();
    if (Current_Char() == '[') /* ']' for balance */
    {
	n = (size_t)Current_Position();
	out_tabular_char((int)'[');
	Advance_Position();
	(void)out_whitespace();
	while (In_Tabular() && /* '[' for balance */ (Current_Char() != ']'))
	{
	    out_tabular_char(Current_Char());
	    Advance_Position();
	    (void)out_whitespace();
	}
	if (Current_Char() == ']')
	{
	    out_tabular_char(Current_Char());
	    Advance_Position();
	}
    }
    else
	n = 0;
    return (n);
}


static size_t
out_position(VOID_ARG)
{
    return (out_bytepos);
}


static size_t
out_row(VOID_ARG)
{	/* return index of next row (advancing Current_Position() to end */
	/* of row, just before \\ or \end..., or after \hline), or else */
	/* 0 (if no more rows) */
    size_t first_cell;

    if (!In_Tabular())
	return (0);

    column = 0;				/* global start of new row */
    (void)out_whitespace();	  	/* take care of pre-row space and */
    delete_spaces();			/* normalize to constant indentation */
    first_cell = out_cell();
    if (strncmp(&tabular_buffer[first_cell],"\\hline",6) == 0)
    {
	delete_spaces();
	out_tabular_string("\\hline\n",7);
	Current_Position() = first_cell + 6; /* special case */
    }
    else
    {
	while (out_cell_separator() > 0)
	    (void)out_cell();
    }
    return (first_cell);
}


static size_t
out_row_separator(VOID_ARG)
{   /* return index of \\*[...] (advancing Current_Position() past it), or 0 */
    size_t separator_start;

    (void)out_whitespace();
    if (strncmp(&Current_Char(),"\\\\",2) == 0)
    {
	if (!isspace(last_char(0)))
	    out_tabular_char((int)' ');
	out_tabular_char((int)'\\');
	out_tabular_char((int)'\\');
	separator_start = (size_t)Current_Position();
	Current_Position() += 2;
	(void)out_whitespace();
	if (Current_Char() == '*')
	{
	    out_tabular_char(Current_Char());
	    Advance_Position();
	}
	(void)out_optional_argument();
	out_tabular_char((int)'\n');
    }
    else
	separator_start = 0;
    return (separator_start);
}


#if STDC
static void
out_tabular_char(int c)
#else
static void
out_tabular_char(c)
int c;
#endif
{
    out_bytepos++;
    out_verbatim_char(c);
    if (c == (int)'\n')
	out_indentation();
}


#if STDC
static void
out_tabular_string(const char *s, size_t n)
#else
static void
out_tabular_string(s, n)
const char *s;
size_t n;
#endif
{
    size_t k;

    for (k = 0; k < n; ++k)
	out_tabular_char((int)s[k]);
}


static size_t
out_whitespace(VOID_ARG)
{   /* output comments verbatim, but reduce all other whitespace to one space */
    size_t n;
    size_t spaces;

    n = (size_t)((In_Tabular() && isspace(Current_Char())) ?
		 Current_Position() : 0);

    for (spaces = 0; In_Tabular() && isspace(Current_Char()); Advance_Position())
	spaces++;			/* skip whitespace */

    if (spaces > 0)
	out_tabular_char((int)' ');

    if (Is_Command('%'))
    {					/* found comment, so output it */
	out_tabular_char((int)Current_Char());
	do				/* advance to newline */
	{
	    Advance_Position();
	    if (!In_Tabular())
		break;
	    out_tabular_char((int)Current_Char());
	} while (Current_Char() != '\n');
	(void)out_whitespace();
    }
    return (n);
}


#if STDC
static void
set_tabular_column_width(int width)
#else
static void
set_tabular_column_width(width)
int width;
#endif
{
    if (column > max_column)
	max_column = column;
    if (width > column_width[column])
	column_width[column] = width;
}
