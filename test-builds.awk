### /u/sy/beebe/src/fpp/fpp-3.0.0/test-builds.awk, Fri May 18 18:42:22 2001
### Edit by Nelson H. F. Beebe <beebe@math.utah.edu>
### ====================================================================
### Extract a compact result summary from build typescripts produced
### from test-build.sh.
###
### Usage:
###	awk -f test-builds.awk typescript(s) > summary-file
###
### [18-May-2001]
### ====================================================================

BEGIN			{ initialize() }

			{ gsub("\r","") } # ignore CR (Ctl-M) in typescripts

(LAST_FILENAME != FILENAME) \
			{ save_results(); LAST_FILENAME = FILENAME; NFILES++ }

/Date:/			{ save_results(); Date = value($0) }

### Hostname:     darwin.math.utah.edu
/^Hostname:/		{ Hostname = $2 }

### Machine type: Apple PowerMac G4 (2 533MHz CPUs; 256MB RAM); Darwin 1.3.3
/^Machine type:/	{ Machine_Type = value($0) }

### System:       Darwin darwin.math.utah.edu 1.3.3 Darwin Kernel Version 1.3.3: Fri May 4 13:46:54 PDT 2001; root:xnu/xnu-124.8.obj~1/RELEASE_PPC Power Macintosh powerpc
/^System:/		{ System = value($0) }

### Compiler:     /usr/local/bin/lcc
/^Compiler:/		{ save_compilers(value($0)) }

### Options:      -DXOPEN_SOURCE
/^Options:/		{ Options = value($0) }

/^[<>]/			{ Errors++ }

/^cmp:/			{ Errors++ }


/^check[0-9]+/		{ Checks++ }

/^checking whether the C compiler.*works.* no/ \
			{ Bad_Compiler = 1 }

/rcc: unknown target/	{ Bad_Compiler = 1 }

/^---/			{ save_results() }

END			{ save_results(); print_report() }


### ====================================================================

function countof(array, key,n)
{
    n = 0
    for (k in array)
	n++
    return (n)
}


function initialize()
{
    Max_Checks = 0
    new_entry()
}


function new_entry()
{
    Bad_Compiler = Checks = Errors = 0
    Compiler = Hostname = Machine_Type = Options = System = ""
}


function print_report( checks,failures,k,machine_type,n,n_failed,n_ok,
		       n_partial,parts,sort_pipe,sorted_types,triple)
{
    sort_pipe = "sort"
    n_failed = n_ok = n_partial = 0
    n = sort_keys(Types,sorted_types)

    print "Success report\n"

    for (k = 1; k <= n; ++k)
    {
	machine_type = sorted_types[k]
	print "--------------------------------------------------------------------------------"
	print ""
	print machine_type
	print Dates[machine_type]
	print ""
	for (triple in Result)
	{
	    split(triple,parts,SUBSEP)
	    if (parts[1] == machine_type)
	    {
		split(Result[triple],checks,"\t")
		printf("%-71s\t%s\n", (parts[2] " " parts[3]), \
		    ((checks[1] == checks[2]) ? "OK" : \
		     ("FAILED " (checks[2] - checks[1])))) | sort_pipe
		if (checks[1] != checks[2])
		    n_partial++
		else
		    n_ok++
	    }
	}
	close(sort_pipe)
    }

    print "\f"
    print "Failure report\n"

    for (k = 1; k <= n; ++k)
    {
	failures = 0
	machine_type = sorted_types[k]
	for (triple in Failure)
	{
	    split(triple,parts,SUBSEP)
	    if (parts[1] == machine_type)
	    {
		if (failures == 0)
		{
		    print "================================================================================"
		    print ""
		    print machine_type
		    print Dates[machine_type]
		    print ""
		}
		failures++
		printf("%-71s\tFAILED\n", (parts[2] " " parts[3])) | sort_pipe
		n_failed++
	    }
	}
	if (failures > 0)
	    close(sort_pipe)
    }

    print "\f"
    print "Test summary\n"
    printf("Host environments:\t%4d\n", NFILES)
    printf("Compiler paths:   \t%4d\n", countof(Compiler_Paths))
    printf("Compiler names:   \t%4d\n", countof(Compiler_Names))
    printf("Complete success: \t%4d\n", n_ok)
    printf("Partial failure:  \t%4d\n", n_partial)
    printf("Complete failure: \t%4d\n", n_failed)
}


function save_compilers(s, n,parts)
{
    Compiler = s
    sub("^.*/beebe/","$HOME/",Compiler)
    Compiler_Paths[Compiler]++
    n = split(Compiler,parts,"/")
    Compiler_Names[parts[n]]++
}



function save_results()
{
    if ((Hostname != "") && (Bad_Compiler == 0))
    {
	Types[Machine_Type]++
	Dates[Machine_Type] = Date
	if ((Errors > 0) || \
	    (Checks == 0) || \
	    ((Max_Checks > 0) && (Checks < Max_Checks)))
	    Failure[Machine_Type,Compiler,Options] = 1
	else if ((Errors == 0) && (Checks > 0))
	{
	    if (Checks > Max_Checks)
		Max_Checks = Checks
	    Result[Machine_Type,Compiler,Options] = (Checks "\t" Max_Checks)
	}
	else
	    Failure[Machine_Type,Compiler,Options] = 1
    }

    ## This call must come OUTSIDE the previous "if () {...}" in case
    ## we had a bad compiler!

    new_entry()
}


function sort_keys(keys,sorted_keys, k,key,m,n)
{
    split("",sorted_keys)		# awk idiom: clear sorted_keys[]

    n = 0
    for (key in keys)
    {
	n++
	sorted_keys[n] = key
    }
    for (k = 1; k < n; ++k)
    {
	for (m = k + 1; m <= n; ++m)
	{
	    if (sorted_keys[k] > sorted_keys[m])
	    {
		key = sorted_keys[m]
		sorted_keys[m] = sorted_keys[k]
		sorted_keys[k] = key
	    }
	}
    }
    return (n)
}


function squeeze_blanks(s)
{
    gsub(/  +/," ",s)		# squeeze out duplicate blanks
    return (s)
}


function trim(s)
{
    sub(/^ +/,"",s)		# trim leading space
    sub(/[, ]+$/,"",s)		# trim trailing space and punctuation
    return (squeeze_blanks(s))
}


function value(s)
{
    sub("^[^:]*:[ \t]*","",s)
    return (trim(s))
}
