#! /bin/sh
### ====================================================================
### Test all known C and C++ compilers for builds and validations of
### an autoconfigurized package, or a single C source file.  Several
### environments are tested for each compiler (POSIX, XOPEN, and
### vendor-specific).
###
### Usage:
###	./test-builds.sh [--srcdir=dirname]
###	./test-builds.sh [--srcdir=dirname] file.c
###
### If the --srcdir option is omitted, then the source directory is
### assumed to be that of this script.
###
### If an existing C filename is named on the command line, then it is
### compiled and linked, but not run.  No configure script or Makefile
### is then necessary.
###
### Otherwise, any configure script is run with CC set to the current
### compiler and options, and then "make CC='...' all check" is run.
###
### [15-May-2001]
### ====================================================================

### Allow the srcdir to be somewhere else, perhaps in a readonly file
### system:
case "$1" in
	--srcdir=* | --srcdi=* | --srcd=* | --src=* | --sr=* | --s=* )
		srcdir=`echo $1 | sed -e 's/^[^=]*=//'`
		shift
		;;
	*)
		srcdir=`dirname $0`
		;;
esac


mywhich()
{
	case $1 in
		/*)		# Handle absolute path case separately
			test -f $1 && echo "$@" && return
			;;
		*)		# else search PATH
			for d in ` echo $PATH | sed -e 's/:/ /g' `
			do
				test -f $d/$1 && echo $d/"$@" && return
			done
			echo ""
			;;
	esac
}

case "`uname -s || true`" in
	AIX*)		ccoptsmore="-D_ALL_SOURCE -D_ANSI_C_SOURCE -D_XOPEN_SOURCE_EXTENDED" ;;
	Darwin*)	ccoptsmore="-D_AUX_SOURCE" ;;
	FreeBSD*)	ccoptsmore="-D_XOPEN_SOURCE_EXTENDED" ;;
	HP-UX*)		ccoptsmore="-D_AES_SOURCE -D_HPUX_C_SOURCE -D_OSF_SOURCE -D_XOPEN_SOURCE_EXTENDED" ;;
	IRIX*)		ccoptsmore="-D_POSIX_C_SOURCE -D_SGI_SOURCE -D_XOPEN_SOURCE_EXTENDED" ;;
	Linux*)		ccoptsmore="-D_BSD_SOURCE -D_GNU_SOURCE -D_ISOC99_SOURCE -D_ISOC99_SOURCE -D_POSIX_C_SOURCE -D_SVID_SOURCE -D_XOPEN_SOURCE_EXTENDED" ;;
	OSF1*)		ccoptsmore="-D_AES_SOURCE -D_OSF_SOURCE" ;;
	Rhapsody*)	ccoptsmore= ;;
	SunOS*)		ccoptsmore="-D_POSIX_C_SOURCE -D_XOPEN_SOURCE_EXTENDED" ;;
	*)		ccoptsmore= ;;
esac

### Loop over all known compilers on local UNIX systems, starting with
### C++ ones, because they often find errors that C compilers miss.
### During gcc-3.0 development, we install compiler snapshots in
### /usr/local/test/bin, so we test those too.
for compiler in \
		"cxx -x cxx" \
		CC \
		DCC \
		NCC \
		g++ \
		/usr/local/test/bin/g++ \
		pgCC \
		sgiCC \
		xlC \
		c89 \
		cc \
		gcc \
		/usr/local/test/bin/gcc \
		lcc \
		pgcc \
		sgicc \
		xlc
do
	CC=`mywhich $compiler`
	if test -n "$CC"
	then
		for ccopts in "" "-D_ANSI_SOURCE" "-D_POSIX_SOURCE" "-D_XOPEN_SOURCE" $ccoptsmore
		do
			echo ------------------------------------------------------------------------
			echo
			echo Begin test builds
			echo "Date:        " `date`
			echo "Directory:   " $PWD
			echo "Hostname:    " `hostname`
			echo "Machine type:" `machinetype || true`
			echo "System:      " `uname -a`
			echo "Compiler:    " $CC
			echo "Options:     " $ccopts
			echo
			test -f Makefile && make distclean > /dev/null 2>&1
			rm -f a.out confdefs.h config.cache config.log config.status *.o *.a > /dev/null 2>&1
			if test -n "$1" -a -f "$1"
			then
				$CC $ccopts $1
				rm -f a.out
			else
				if test -f $srcdir/configure
				then
					echo time env CC='"'$CC $ccopts'"' $srcdir/configure
					time env CC="$CC $ccopts" $srcdir/configure
				fi
				echo
				echo time make CC='"'$CC $ccopts'"' all '&&' time make CC='"'$CC $ccopts'"' check
				time make CC="$CC $ccopts" all && time make CC="$CC $ccopts" check
			fi
			echo
		done
	fi
done
echo ========================================================================
