% -*-LaTeX-*-
% This large file contains an instance of every macro included in the
% internal tables in tex-pretty.  It certainly cannot be given to TeX,
% because the constructions are mostly nonsensical.  Its sole purpose
% is to test the formatting supplied by tex-pretty.  The input lines
% are intentionally made very long.  They are pattern after the
% builtin_style[] initializers, except that sample required arguments
% for verbatim macros have been supplied, and begin/end pairs have
% been placed consecutively with a small piece of enclosed text.
% [06-Jun-1995]


\verb=/* AmSLaTeX (extension of LaTeX) */=

{\bf newline-before} ABC... \DeclareMathOperator \DeclareMathSymbol \DeclareSymbolFont \numberwithin \swapnumbers \newtheoremstyle \theoremstyle ...XYZ


\verb=/* AmSLaTeX */=

{\bf standalone} ABC... \allowdisplaybreaks ...XYZ


\verb=/* AmSTeX */=

{\bf environment} ABC... \CD xxx \endCD \Refs xxx \endRefs \Sb xxx \endSb \Sp xxx \endSp \Vmatrix xxx \endVmatrix \abstract xxx \endabstract \affil xxx \endaffil \align xxx \endalign \alignat xxx \endalignat \aligned xxx \endaligned \alignedat xxx \endalignedat \block xxx \endblock \bmatrix xxx \endbmatrix \cases xxx \endcases \cfrac xxx \endcfrac \comment xxx \endcomment \curraddr xxx \endcurraddr \dedicatory xxx \enddedicatory \definition xxx \enddefinition \demo xxx \enddemo \document xxx \enddocument \email xxx \endemail \example xxx \endexample \gather xxx \endgather \gathered xxx \endgathered \graf xxx \endgraf \head xxx \endhead \keywords xxx \endkeywords \matrix xxx \endmatrix \multline xxx \endmultline \pmatrix xxx \endpmatrix \proclaim xxx \endproclaim \remark xxx \endremark \smallmatrix xxx \endsmallmatrix \specialhead xxx \endspecialhead \split xxx \endsplit \subhead xxx \endsubhead \subjclass xxx \endsubjclass \subsubhead xxx \endsubsubhead \toc xxx \endtoc \topmatter xxx \endtopmatter \translator xxx \endtranslator \vmatrix xxx \endvmatrix \xalignat xxx \endxalignat \xxalignat xxx \endxxalignat ...XYZ


\verb=/* AmSTeX */=

{\bf footnote} ABC... \plainfootnote \plainproclaim ...XYZ


\verb=/* AmSTeX */=

{\bf list} ABC... \roster \runinitem zero \item one \item two \itemitem{$\bullet$} two-sub-one \itemitem{$\bullet$} two-sub-two \endroster ...XYZ


\verb=/* AmSTeX */=

{\bf list-item} ABC... \runinitem ...XYZ


\verb=/* AmSTeX conflicts with LaTeX: LaTeX takes precedence, and AmSTeX */=

\verb=/* loses some leading indentation */=

{\bf newline-before} ABC... \endaddress \endauthor \endcaption \enddate \endref \endthanks \endtitle ...XYZ


\verb=/* AmSTeX */=

{\bf newline-before} ABC... \adjustfootnotemark \book \bookinfo \bookinquotes \botcaption \by \bysame \captionwidth \define \ed \eds \finalinfo \inbook \issue \jour \lang \moreref \newsymbol \no \noquotes \page \pages \paper \paperinfo \paperinquotes \parshape \preaffil \preauthor \predate \predefine \prepaper \pretitle \publ \publaddr \redefine \shoveleft \shoveright \toappear \topcaption \transl \undefine \vol \yr ...XYZ


\verb=/* AmSTeX */=

{\bf standalone} ABC... \BlackBoxes \CenteredTagsOnSplits \LimitsOnInts \LimitsOnNames \LimitsOnSums \Monograph \NoBlackBoxes \NoPageNumbers \NoRunningHeads \Runinitem \TagsAsMath \TagsAsText \TagsOnLeft \TagsOnRight \TopOrBottomTagsOnSplits \UseAMSsymbols \UseBibTeX \endinsert \foldedpar \galleys \ininbook \loadbold \loadeufb \loadeufm \loadeurb \loadeurm \loadeusb \loadeusm \loadmsam \loadmsbm \midinsert \pageinsert \printoptions \showallocations \syntax \topinsert ...XYZ


\verb=/* Extended plain TeX (ETeX) */=

{\bf list} ABC... \numberedlist xxx \endnumberedlist \orderedlist xxx \endorderedlist \unorderedlist xxx \endunorderedlist ...XYZ


\verb=/* ETeX */=

{\bf footnote} ABC...\numberedfootnote{This is a footnote.} ...XYZ


\verb=/* ETeX */=

{\bf index} ABC...\idx{idx entry}\idxmarked{idxmarked entry}\idxname{idxname entry}\idxsubmarked{idxsubmarked entry}\sidx{sidx entry}\sidxmarked{sidxmarked entry}\sidxname{sidxname entry}\sidxsubmarked{sidxsubmarked entry} ...XYZ


\verb=/* ETeX */=

{\bf newline-before} ABC... \defineindex \definecontentsfile \center \columnfill \doublecolumns \edefappend \flushleft \flushright \for \iffileexists \innerdef \innerinnerdef \innernewbox \innernewcount \innernewdimen \innernewfam \innernewhelp \innernewif \innernewinsert \innernewmuskip \innernewread \innernewskip \innernewtoks \innernewwrite \listing \makecolumns \quadcolumns \readindexfile \readtocfile \testfileexistence \tocchapterentry \tocsectionentry \triplecolumns \writenumberedtocentry \writetocentry ...XYZ


\verb=/* ETeX */=

{\bf verb} ABC... \verbatim| ab||cd |endverbatim \verbatim| 
ab||cd 
|endverbatim
...XYZ


\verb=/* LAmSTeX */=

{\bf chapter} ABC... \docstyle \subtopic \topic ...XYZ


\verb=/* LAmSTeX */=

{\bf environment} ABC... \Figure xxx \endFigure \Figurepair xxx \endFigurepair \Figuretriple xxx \endFiguretriple \HL xxx \endHL \Table xxx \endTable \bdmatrix xxx \endbdmatrix \claim xxx \endclaim \heading xxx \endheading \island xxx \endisland \makebib xxx \endmakebib \partition xxx \endpartition  ...XYZ


\verb=/* LAmSTeX */=

{\bf list} ABC... \bib xxx \endbib \bullist xxx \endbullist \describe xxx \enddescribe \list xxx \endlist \margins xxx \endmargins ...XYZ



\verb=/* LAmSTeX */=

{\bf newline-before} ABC... \Cgaps \Entry \Entryxref \LETTER \Morexref \Noexpand \Nonexpanding \Page \PageSpan \Pagespan \PostCDSpace \PreCDSpace \Rgaps \Topage \Xref \cgaps \cleartable \counter \everytable \ex \exs \flushpar \fnote \foottext \hL \hdashed \hl \hls \htablelines \iabbrev \idefine \litbackslash \litdelimiter \mainfile \makepiece \manyby \measuretable \modifyfootnote \nameHL \namehl \newclaim \newisland \note \pageorder \postCDspace \postdocstyle \preCDspace \predocstyle \pullin \pullinmore \purge \readaux \rgaps \runningchapter \runningsection \shortenclaim \showstored \sss \storetable \tablewidth \tbldocstyle \tdefine \toclevel \tredefine \tss \unpurge \usetable \vleft \vright \vs \vsolid \vtablelines ...XYZ


\verb=/* LAmSTeX */=

{\bf standalone} ABC... \Figureproofing \FlushedFigs \Initialize \NS \NoFlushedFigs \RefWarnings \alldq \boxedtables \columnbreak \continuelist \figureproofing \indexfile \indexproofing \inlevel \keepitem \makelistFigures \makelistTables \makelistfigures \makelisttables \maketoc \newcolumn \noFigureproofing \nocolumnbreak \nofigureproofing \noshowsecondpass \opentables \outlevel \shortlastcolumn \showcolwidths \showsecondpass \sides \ssizeCDlabels \tocfile \tsizeCDlabels ...XYZ


\verb=/* LAmSTeX */=

{\bf verb} ABC... \Lit*Literal text* \lit*more literal text* \Lit*Literal
text
with
embedded
newlines
*
...XYZ


\verb=/* LaTeX and LAmSTeX */=

{\bf chapter} ABC... \appendix \backmatter \chapter \documentclass \documentstyle \frontmatter \mainmatter \paragraph \part \section \subparagraph \subsection \subsubsection ...XYZ


\verb=/* LaTeX and LAmSTeX */=

{\bf cite} ABC... \cite \nocite ...XYZ


\verb=/* LaTeX */=

{\bf footnote} ABC... \footnotetext \label \pageref \ref ...XYZ


\verb=/* LaTeX */=

{\bf index} ABC...\glossary{glossary entry}\index{index entry} ...XYZ


\verb=/* LaTeX */=

{\bf newline-after} ABC... \kill ...XYZ


\verb=/* LaTeX and LAmSTeX */=

{\bf newline-before} ABC... \author \date \thanks \title ...XYZ


\verb=/* LaTeX */=

{\bf newline-before} ABC... \address \caption \closing \glossaryentry \include \includeonly \indexentry \makeglossary \makeindex \marginpar \markboth \markright \multiput \newblock \newboolean \newcommand \newcounter \newenvironment \newlength \newsavebox \newtheorem \opening \printindex \providecommand \put \renewcommand \renewenvironment \signature \typein \typeout \usepackage \vspace ...XYZ


\verb=/* LaTeX */=

{\bf standalone} ABC... \bigpagebreak \cleardoublepage \clearpage \flushbottom \fussy \hline \indexspace \listfiles \listoffigures \listoftables \maketitle \medpagebreak \newline \newpage \nofiles \normalbottom \normalmarginpar \onecolumn \onlynotes \onlyslides \raggedleft \reversemarginpar \sloppy \smallpagebreak \tableofcontents \twocolumn ...XYZ


\verb=/* LaTeX */=

{\bf verb} ABC... \path=Jane.Q.Doe@very.long.hostname.on.the.Internet= \verb@This is an inline verbatim string@ ...XYZ


\verb=/* plain TeX */=

{\bf chapter} ABC... \beginchapter \beginsection \endchapter ...XYZ


\verb=/* plain TeX */=

{\bf environment} ABC... \begingroup xxx \endgroup \bgroup xxx \egroup  ...XYZ


\verb=/* plain TeX */=

{\bf footnote} ABC... \or ...XYZ


\verb=/* plain TeX */=

{\bf footnote} ABC... \footnote \vfootnote ...XYZ


\verb=/* plain TeX */=

{\bf newline-after} ABC... \cr \crcr \endline ...XYZ


\verb=/* plain TeX */=

{\bf newline-before} ABC... \chardef \closein \closeout \countdef \def \dimendef \edef \else \endinput \eqalign \eqalignno \errmessage \fi \futurelet \futurenonspacelet \gdef \global \halign \hang \hoffset \hyphenation \ialign \if \ifcase \ifcat \ifdim \ifeof \iffalse \ifhbox \ifhmode \ifinner \ifmmode \ifnum \ifodd \iftrue \ifundefined \ifvbox \ifvmode \ifvoid \ifx \immediate \input \leqalignno \listing \loop \magnifiction \mark \mathchardef \message \narrower \newbox \newcount \newdimen \newfam \newhelp ...XYZ


\verb=/* plain TeX */=

{\bf newline-before} ABC... \newif \newinsert \newmuskip \newlanguage \newread \newskip \newtoks \newwrite \noalign \openin \openout \read \repeat \show \showbox \showboxbreadth \showdepth \showhyphens \showlists \showthe \skipdef \special \tabalign \textindent \toksdef \tracingall \tracingcommands \tracinglostchars \tracingmacros \tracingonline \tracingoutput \tracingpages \tracingparagraphs \tracingrestores \tracingstats \vadjust \valign \voffset \vskip \write \centerline \leftline \line \parindent \rightline \xdef ...XYZ


\verb=/* plain TeX */=

{\bf standalone} ABC... \allowbreak \annotations \batchmode \bigbreak \bigskip \body \break \bye \centering \dosupereject \eject \endletter \errorstopmode \filbreak \frenchspacing \goodbreak \indent \leavevmode \makelabel \medbreak \medskip \noindent \nonfrenchspacing \nonstopmode \par \raggedbottom \raggedcenter \raggedright \removelastskip \scrollmode \smallbreak \smallskip \ttraggedright \vfil \vfilneg \vfill ...XYZ
